const Profile = () => import('@/components/User/Profile')

export default {
    name: 'users.profile',
    path: 'profile',
    component: Profile,
    meta: {
        breadcrumb: 'profile',
        title: 'Profile User',
        rule: 'isLogged'
    }
}
