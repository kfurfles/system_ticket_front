import routeImporter from '@/modules/importers/routeImporter'

const routes = routeImporter(require.context('./Users', false, /.*\.js$/))
import RouterView from '@/containers/Router'

export default {
    path: 'users',
    component: RouterView,
    meta: {
        breadcrumb: 'users',
        route: 'users.index'
    },
    children: routes
}
