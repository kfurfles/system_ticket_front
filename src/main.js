// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
const App = () => import('./App')
import acl from '@/plugins/acl'
import * as firebase from 'firebase'
import config from './config-firebase'
import router from './router.js'
import store from './store'
const AlertCmp = () => import('./components/Shared/Alert.vue')

Vue.use(Vuetify)
Vue.config.productionTip = false
console.log()
Vue.component('app-alert', AlertCmp)
/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    acl,
    template: '<App/>',
    components: { App },
    created () {
        firebase.initializeApp(config)
        firebase.auth().onAuthStateChanged(async (user) => {
            if (user) {
                await this.$store.dispatch('autoSignIn', user)
                this.$router.push({ path: '/' })
            } else {
                this.$router.push({ path: '/login' })
            }
        })
    }
})
