const Signin = () => import('@/components/User/Signin')

export default {
    name: 'users.signin',
    path: 'signin',
    component: Signin,
    meta: {
        breadcrumb: 'signin',
        title: 'Signin User',
        guestGuard: true
    }
}
