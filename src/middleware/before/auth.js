export default (to, from, next) => {
    console.log(to)
    if (to.meta.guestGuard) {
        next({ path: '/' })
    } else {
        next()
    }
}
