import Login from '@/views/Auth/Login'
import Logout from '@/views/Auth/Logout'

export default [
    {
        name: 'login',
        path: '/login',
        component: Login,
        meta: {
            rule: 'isPublic',
            title: 'Login'
        }
    },
    {
        name: 'logout',
        path: '/logout',
        component: Logout,
        meta: {
            rule: 'isPublic',
            title: 'Logout'
        }
    }
]
