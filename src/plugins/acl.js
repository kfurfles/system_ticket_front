import Vue from 'vue'
import { AclInstaller, AclCreate, AclRule } from 'vue-acl'
import router from '@/router'

const ROLES = {
    ADMIN: 'admin',
    PUBLIC: 'public',
    USER: 'user'
}

Vue.use(AclInstaller)

export default new AclCreate({
    initial: ROLES.PUBLIC,
    notfound: '/notfound',
    router,
    acceptLocalRules: true,
    globalRules: {
        isAdmin: new AclRule(ROLES.ADMIN).generate(),
        isUser: new AclRule(ROLES.USER).generate(),
        isPublic: new AclRule(ROLES.PUBLIC).or(ROLES.ADMIN).or(ROLES.USER).generate(),
        isLogged: new AclRule(ROLES.USER).or(ROLES.PUBLIC).or(ROLES.ADMIN)
    }
})
