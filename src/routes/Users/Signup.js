const Signup = () => import('@/components/User/Signup')

export default {
    name: 'users.signup',
    path: 'signup',
    component: Signup,
    meta: {
        breadcrumb: 'signup',
        title: 'Signup User',
        guestGuard: true
    }
}
