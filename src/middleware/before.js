// import store from './../store'
// import guest from './before/guest'
// import auth from './before/auth'
// import allow from './before/allow'
// import nprogress from './before/nprogress';

export default (to, from, next) => {
    // nprogress();
    // console.log(to)
    // console.log(from)
    // next()
    if (store.state.user) {
        auth(to, from, next)
        // allow(to, from, next)
    } else {
        next({ path: '/login' })
    }
}
