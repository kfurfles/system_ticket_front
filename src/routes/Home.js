import routeImporter from '@/modules/importers/routeImporter'

const routes = routeImporter(require.context('./Home', false, /.*\.js$/))
import RouterView from '@/containers/Router'

export default {
    path: '',
    component: RouterView,
    meta: {
        breadcrumb: 'home',
        route: 'home.index',
    },
    children: routes
}
