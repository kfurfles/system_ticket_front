import Vue from 'vue'
import Router from 'vue-router'

import routeImporter from '@/modules/importers/routeImporter'

import before from './middleware/before'

const routes = routeImporter(require.context('./routes', false, /.*\.js$/))

Vue.use(Router)

const router = new Router({
    routes,
    mode: 'history'
})

// router.beforeEach(before)
// router.afterEach(after)

export default router
