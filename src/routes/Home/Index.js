const Home = () => import('@/views/Home')

export default {
    name: 'index',
    path: '/',
    component: Home,
    meta: {
        breadcrumb: 'Home',
        title: 'Home',
        rule: 'isLogged'
    }
}
