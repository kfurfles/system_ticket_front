import Vue from 'vue'
import Vuex from 'vuex'
import user from './user'
import shared from './shared'

import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex)

export default new Vuex.Store({
    // plugins: [createPersistedState()],
    modules: {
        user: user,
        shared: shared
    }
})
